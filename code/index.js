import { pizzaSelectSize, pizzaSelectTopping, show, drag, delTopping, delSauce, moving, validate } from "./functions.js";
import { pizzaUser } from "./data-pizza.js"

let nameNum = 0, phoneNum = 0, emailNum = 0

document.querySelectorAll(".grid input")
    .forEach((input) => {
        if (input.type === "text" || input.type === "tel" || input.type === "email") {
            input.addEventListener("blur", () => {
                if (input.type === "text" && validate(/^[А-я-іїґє]{2,}$/i, input.value)) {
                    selectInputName(input, pizzaUser)
                    nameNum = 1;
                } else if (input.type === "tel" && validate(/^\+380\d{9}$/, input.value)) {
                    selectInputTel(input, pizzaUser)
                    phoneNum = 1;
                } else if (input.type === "email" && validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, input.value)) {
                    selectInputEmail(input, pizzaUser)
                    emailNum = 1;
                } else {
                    input.classList.add("error")
                }
            })
        } else if (input.type === "reset") {
            input.addEventListener("click", () => {

            })
        } else if (input.type === "button") {
            input.addEventListener("click", () => {
                localStorage.userInfo = JSON.stringify(pizzaUser);
                if (nameNum + phoneNum + emailNum == 3) {
                    //console.log("hehe")
                    setTimeout( 'location="../thank-you/index.html";', 1000 );
                }
            })
        }
    })

  



function selectInputName(input, data) {
    input.className = ""
    input.classList.add("success")
    data.userName = input.value
}
function selectInputTel(input, data) {
    input.className = ""
    input.classList.add("success")
    data.userPhone = input.value
}
function selectInputEmail(input, data) {
    input.className = ""
    input.classList.add("success")
    data.userEmail = input.value
}




document.querySelector("#pizza")
    .addEventListener("click", pizzaSelectSize)

document.querySelector(".ingridients")
    .addEventListener("click", pizzaSelectTopping)

show(pizzaUser)

document.querySelector('#banner').addEventListener('mouseover', moving)
document.body.addEventListener('dragstart', drag)
document.body.addEventListener('click', delTopping)
document.body.addEventListener('click', delSauce)
