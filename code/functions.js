import { pizzaUser, pizzaBD } from "./data-pizza.js";

const table = document.querySelector(".table")

const pizzaSelectSize = (e) => {
    if (e.target.tagName === "INPUT" && e.target.checked) {
        pizzaUser.size = pizzaBD.size.find(el => el.name === e.target.id);
    }
    show(pizzaUser)
};

const pizzaSelectTopping = (e) => {
    if (!e.type) e.target = e;
    switch (e.target.id) {
        case "sauceClassic": pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        case "sauceBBQ": pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        case "sauceRikotta": pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === e.target.id);
            break;
        default:
            if (pizzaUser.topping.length) {
                if (pizzaUser.topping.find(el => el === pizzaBD.topping.find(el => el.name === e.target.id))) {
                    pizzaUser.topping.map(el => {
                        if (el.name === e.target.id) el.count = (el.count || 0) + 1;
                    })
                } else {
                    let el = pizzaBD.topping.find(el => el.name === e.target.id);
                    el.count = 1;
                    pizzaUser.topping.push(el);
                }
            } else {
                pizzaUser.topping.push(pizzaBD.topping.find(el => el.name === e.target.id));
                pizzaUser.topping[0].count = 1;
            }
            break;
    }
    show(pizzaUser);
    if (e.target.tagName === "IMG") {
        if (e.target.id.match('sauce')) {
            const sauce = document.querySelector(".sauce-layer")
            sauce.innerHTML = `<img src="${e.target.src}" data-name="${e.target.id}">`;
        } else {
            table.insertAdjacentHTML("beforeend", `<img src="${e.target.src}" data-name="${e.target.id}">`)
        }
    }
}


function show(pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping")

    let totalPrice = 0;
    if (pizza.size !== "") {
        totalPrice += parseFloat(pizza.size.price);
    }
    if (pizza.sauce !== "") {
        totalPrice += parseFloat(pizza.sauce.price);
    }
    if (pizza.topping.length) {
        totalPrice += pizza.topping.reduce((a, b) => a + (b.price * b.count), 0)
    }
    price.innerText = totalPrice;

    if (pizza.sauce !== "") {
        sauce.innerHTML = `<span class="topping">${pizza.sauce.productName}<button type="button" class="sauce-del"></button></span>`
    }

    if (Array.isArray(pizza.topping)) {
        topping.innerHTML = pizza.topping.map(el => {
            if (el.count > 1) {
                return `<span class="topping" data-count=${el.count}>${el.productName}<button type="button" class="topping-del"></button></span>`
            } else {
                return `<span class="topping">${el.productName}<button type="button" class="topping-del"></button></span>`
            }
        }).join("");
    }

    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date()
}

// 
function drag(e) {
    if (e.target.closest('.ingridients')) {
        let dragElem = e.target

        table.addEventListener('dragleave', dragParts)
        function dragParts() {
            table.removeEventListener('dragleave', dragParts);
            pizzaSelectTopping(dragElem);
        }
    }
}


function delSauce(e) {
    if (e.target.classList.contains('sauce-del')) {
        pizzaUser.price -= pizzaUser.sauce.price;
        price.innerText = pizzaUser.price;
        pizzaUser.sauce = '';
        document.querySelector('[data-name*=sauce]').remove();
        document.getElementById("sauce").innerHTML = '';
    }
}

function delTopping(e) {
    if (e.target.closest('.topping-del')) {
        let btn = e.target.closest('.topping-del');
        let currentTopping = btn.parentElement
        let type = pizzaBD.topping.find(el => el.productName === currentTopping.textContent);

        currentTopping.dataset.count ? currentTopping.dataset.count -= 1 : currentTopping.dataset.count = 0;
        pizzaUser.topping.map(el => {
            if (el.productName === currentTopping.textContent) {
                let currentImg = document.querySelectorAll(`[data-name='${el.name}']`);

                currentImg[currentImg.length - 1].remove();
                el.count -= 1;
            }
        })
        if (currentTopping.dataset.count == 0) {
            pizzaUser.topping.splice(pizzaUser.topping.indexOf(type), 1)
            currentTopping.remove();
        }

        pizzaUser.price -= type.price;
        price.innerText = pizzaUser.price;
    }
}



function moving(e) {
    
let elemRight, elemBottom, maxElemRight, maxElemBottom, elem;

elem = document.getElementById('banner');

maxElemRight = document.documentElement.clientWidth - elem.offsetWidth;
maxElemBottom = document.documentElement.clientHeight - elem.offsetHeight;

elem.onmousemove = handler;

function handler() {
  elemRight = Math.random() * maxElemRight;
  elem.style.right = elemRight + 'px';
  elemBottom = Math.random() * maxElemBottom;
  elem.style.bottom = elemBottom + 'px';

}
}

const validate = (pattern, value) => pattern.test(value);

export { pizzaSelectSize, pizzaSelectTopping, show, drag, delTopping, delSauce, moving, validate }